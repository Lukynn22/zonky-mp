/**
 * Created by Lukáš Materna
 * Date: 20.02.17
 */

import {Component} from 'react';
import 'whatwg-fetch';

export default class RequestManager extends Component {
    constructor(query, params, callback) {
        super(query, params, callback);

        var body = {
            //id_account: AuthStore.getIdAccount(),
            ...params,
        };

        //?rating__eq=AA
        return fetch('http://zonky.actimer.cz/'+query, {
                method: 'POST',
                header: {
                    'Content-type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                },
                body: JSON.stringify(body)
            })
            .then((response) => response.json())
            .then((responseJson) => {
                try
                {
                    if (responseJson.length <= 0) {
                        callback("", []);
                        return false;
                    }

                    callback("", responseJson);

                }
                catch(e)
                {
                    callback(responseJson, []);
                }

            })
            .catch((error) => {
                callback(error, []);
            });
    }
}
