import React, { Component } from 'react';

import * as BoardActions from '../actions/BoardActions';
import BoardStore from '../stores/BoardStore';

import '../assets/stylesheets/board.css';

export default class Board extends Component {
  constructor(state) {
      super(state);
      this.state = {
          marketplaces: [],
          ratings: ['AAAA', 'AAA', 'AA', 'A', 'B', 'C', 'D'],
          averageAmount: 0,
          featureFlagSelection: true, //Feature flag for displaying of select/input
      };
      this.fetchMarketplace = this.fetchMarketplace.bind(this);
  }

  componentWillMount() {
      BoardStore.on("change", this.fetchMarketplace);
      BoardActions.fetchMarketplace();
  }

  componentWillUnmount() {
      BoardStore.removeListener("change", this.fetchMarketplace);
  }

  fetchMarketplace() {
      var totalAmount = 0;

      this.setState({
          marketplaces: BoardStore.getMarketplace(),
      });

      for (var i = 0; i < this.state.marketplaces.length; i++) {
        totalAmount += this.state.marketplaces[i]['amount'];
      }

      this.setState({
          averageAmount: totalAmount/this.state.marketplaces.length,
      });
  }

  handleGetAvgAmount(event) {
    //FF condition
    var rating = "";
    if (this.state.featureFlagSelection) {
      rating = (event.target.value > 0 ? this.state.ratings[(parseInt(event.target.value,0)-1)] : undefined);

    } else {
      rating = event.target.value;
    }
    BoardActions.fetchMarketplace(rating);
  }

  /**
   * Amount formatting for CZK
   * @param  int amount
   * @return int
   */
  formattAmount(amount) {
    return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " Kč";
  }

  /**
   * Get object of ration input select/input
   * @return Object
   */
  fetchRatingInput() {
    //FF condition
    if (this.state.featureFlagSelection) {
      var ratingSelection = Object.keys(this.state.ratings).map((rating) =>
        <option key={rating} value={(parseInt(rating,0)+1)}>
          {this.state.ratings[rating]}
        </option>
      );

      return <select name="value" onChange={this.handleGetAvgAmount.bind(this)}> 
        <option key="0" value="0">Všechny</option>
        {ratingSelection}
      </select>

    } else {
      return <input name="value" onChange={this.handleGetAvgAmount.bind(this)} />;
    }
  }

  /**
   * Render method
   * @return Object
   */
  render() {
    var ratingSelection = this.fetchRatingInput();

    return (
      <div className="board">
        <div className="form-box">
          <div className="inner-box">
            <div className="box-content">
              <h1>Zonky marketplace</h1>
              <div className="inline-block">
                <span className="span-label">Vybraný rating pro výpočet:</span>
                {ratingSelection}
              </div>
              <div className="inline-block totalAmount">
                <span className="span-label">Průměrná výše půjček:</span>
                <strong>{this.formattAmount(this.state.averageAmount)}</strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
