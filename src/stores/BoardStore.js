import EventEmiter from 'events';
import Dispatcher from '../Dispatcher';

class BoardStore extends EventEmiter {
    constructor() {
        super();
        this.marketplace = [];
    }

    getMarketplace() {
        return this.marketplace;
    }

    handleActions(action) {
        switch(action.type) {
            case "FETCH_MARKETPLACE": {
                this.marketplace = action.marketplace;
                this.emit("change");
                break;
            }
            default: {}
        }
    }
}

var boardStore = new BoardStore();
Dispatcher.register(boardStore.handleActions.bind(boardStore));

window.dispatcher = Dispatcher;
window.boardStore = boardStore;
export default boardStore;
