import dispatcher from "../Dispatcher";
import RequestManager from "../RequestManager";

export function fetchMarketplace(rating) {
    new RequestManager((rating !== undefined ? '?rating__eq='+rating : ''), {}, returnMarketplace);
}

function returnMarketplace(error, state) {
    dispatcher.dispatch({type: 'FETCH_MARKETPLACE', marketplace: state});
}
