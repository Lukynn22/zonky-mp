var gulp = require('gulp');
var compass = require('gulp-compass');

gulp.task('compass', function() {
  gulp.src('src/assets/sass/**/*.scss')
    .pipe(compass({
      config_file: 'config.rb',
      css: 'src/assets/stylesheets',
      sass: 'src/assets/sass'
    }))
    .pipe(gulp.dest('src/assets/stylesheets'));
});

gulp.task('watch', ['compass'], function(){
  gulp.watch('src/assets/sass/**/*.scss', ['compass']); 
});
